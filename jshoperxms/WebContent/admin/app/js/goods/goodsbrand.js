var mModule=angular.module("goodsbrandModule",[]);

/**
 * 货物规格路由控制
 */
mModule.config(function($routeProvider){
	$routeProvider
	.when('/goodsbrandment',{
		templateUrl:'../admin/app/goods/goodsbrandment.html',
		controller:'goodsbrand'
	});
});

mModule.controller('goodsbrand',['$scope','$http',function($scope,$http){
	$scope.title="商品品牌列表";
}]);

