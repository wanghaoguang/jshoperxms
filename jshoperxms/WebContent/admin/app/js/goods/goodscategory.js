var mModule=angular.module("goodscategoryModule",[]);

/**
 * 货物规格路由控制
 */
mModule.config(function($routeProvider){
	$routeProvider
	.when('/goodscategoryment',{
		templateUrl:'../admin/app/goods/goodscategoryment.html',
		controller:'goodscategory'
	});
});

mModule.controller('goodscategory',['$scope','$http',function($scope,$http){
	$scope.title="商品分类列表";
}]);

