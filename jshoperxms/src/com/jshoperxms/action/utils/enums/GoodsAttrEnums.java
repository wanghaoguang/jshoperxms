package com.jshoperxms.action.utils.enums;

import com.jshoperxms.action.utils.enums.BaseEnums.OrderIsInvoice;

/**
 * 商品属性枚举
* @ClassName: GoodsAttrEnums 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author jcchen
* @date 2015年10月9日 下午2:38:33 
*
 */
public class GoodsAttrEnums {

	/**
	 * 属性类型
	* @ClassName: AttrType 
	* @Description: TODO(这里用一句话描述这个类的作用) 
	* @author jcchen
	* @date 2015年10月9日 下午2:40:28 
	*
	 */
	public enum AttrType {
		TEXT("文本", "1");
		private String name;
		private String state;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}


		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		private AttrType(String name, String state) {
			this.name = name;
			this.state = state;
		}

		public static String getName(String state) {
			for (AttrType a : AttrType.values()) {
				if (a.getState().equals(state)) {
					return a.getName();
				}
			}
			return "";
		}
	}
	
	

	/**
	 * 属性是否支持搜索
	* @ClassName: AttrIssearch 
	* @Description: TODO(这里用一句话描述这个类的作用) 
	* @author jcchen
	* @date 2015年10月9日 下午2:40:28 
	*
	 */
	public enum AttrIsSearch {
		SUPPORT("支持", "1"),UNSUPPORT("未支持","0");
		private String name;
		private String state;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}


		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		private AttrIsSearch(String name, String state) {
			this.name = name;
			this.state = state;
		}

		public static String getName(String state) {
			for (AttrIsSearch a : AttrIsSearch.values()) {
				if (a.getState().equals(state)) {
					return a.getName();
				}
			}
			return "";
		}
	}
	
	
	/**
	 * 属性是否支持关联搜索
	* @ClassName: AttrIssearch 
	* @Description: TODO(这里用一句话描述这个类的作用) 
	* @author jcchen
	* @date 2015年10月9日 下午2:40:28 
	*
	 */
	public enum AttrIsSameToLink {
		SUPPORT("支持", "1"),UNSUPPORT("未支持","0");
		private String name;
		private String state;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}


		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		private AttrIsSameToLink(String name, String state) {
			this.name = name;
			this.state = state;
		}

		public static String getName(String state) {
			for (AttrIsSameToLink a : AttrIsSameToLink.values()) {
				if (a.getState().equals(state)) {
					return a.getName();
				}
			}
			return "";
		}
	}
	
	
}
