package com.jshoperxms.action.utils.fileupload;

/**
 * 上传的文件夹名称
* @ClassName: FolderName 
* @Description: 定义上传的文件夹名称
* @author jcchen
* @date 2015年11月3日 下午6:09:23 
*
 */
public class FolderName {
	public final static String UPLOADFOLDER="uploads";
	
}
