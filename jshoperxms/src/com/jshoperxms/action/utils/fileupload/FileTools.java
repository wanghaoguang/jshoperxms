package com.jshoperxms.action.utils.fileupload;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.struts2.ServletActionContext;

import com.jshoperxms.action.utils.statickey.StaticKey;

/**
 * 文件目录检测相关方法
 * 
 * @ClassName: FileTools
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author jcchen
 * @date 2015年11月3日 下午6:10:36
 *
 */
public class FileTools {

	/**
	 * 检测目录是否存在
	 * 
	 * @return
	 */
	public static String isExistdir() {
		String nowTimeStr = StaticKey.EMPTY;
		String savedir = FolderName.UPLOADFOLDER + File.separator;
		String realpath = "";
		SimpleDateFormat sDateFormat;
		sDateFormat = new SimpleDateFormat(StaticKey.DF_YYYYMMDD);
		nowTimeStr = sDateFormat.format(new Date());
		String savePath = ServletActionContext.getServletContext().getRealPath(
				File.separator);
		savePath = savePath + savedir + nowTimeStr + File.separator;
		File dir = new File(savePath);
		if (!dir.exists()) {
			dir.mkdirs();
			realpath = savedir + nowTimeStr + File.separator;
			return realpath;
		} else {
			realpath = savedir + nowTimeStr + File.separator;
			return realpath;
		}
	}
}
