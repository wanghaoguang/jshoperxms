package com.jshoperxms.action.mall.backstage.base;

import org.apache.struts2.json.annotations.JSON;

import com.jshoperxms.service.impl.Serial;
import com.opensymphony.xwork2.ActionSupport;

public class BaseTAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	
	private Serial serial;

	/**
	 * action 返回类型
	 */
	public final static String JSON="json";

	
	@JSON(serialize = false)
	public Serial getSerial() {
		return serial;
	}

	public void setSerial(Serial serial) {
		this.serial = serial;
	}
	
	
}
