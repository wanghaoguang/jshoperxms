package com.jshoperxms.action.mall.backstage.feedback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.InterceptorRef;
import org.apache.struts2.convention.annotation.InterceptorRefs;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.jshoperxms.action.mall.backstage.base.BaseTAction;
import com.jshoperxms.action.utils.BaseTools;
import com.jshoperxms.action.utils.enums.BaseEnums;
import com.jshoperxms.action.utils.enums.BaseEnums.DataUsingState;
import com.jshoperxms.action.utils.statickey.StaticKey;
import com.jshoperxms.entity.PostsT;
import com.jshoperxms.entity.UserT;
import com.jshoperxms.service.PostsTService;
import com.jshoperxms.service.UsertService;
import com.jshoperxms.service.impl.Serial;

@Namespace("/mall/feedback/posts")
@ParentPackage("jshoperxms")
@InterceptorRefs({ @InterceptorRef("mallstack") })
public class PostsTAction extends BaseTAction {

	@Resource
	private PostsTService postsTService;

	@Resource
	private UsertService usertService;
	
	@Action(value = "save", results = { @Result(name = "json", type = "json") })
	public String savePostsT() {
		Criterion criterion = Restrictions.eq("userid", BaseTools.getAdminCreateId());
		UserT usert = this.usertService.findOneByCriteria(UserT.class, criterion);
		PostsT pt = new PostsT();
		pt.setId(this.getSerial().Serialid(Serial.POSTST));
		pt.setTitle(this.getTitle());
		pt.setContent(this.getContent());
		pt.setLoginname(usert.getRealname());
		pt.setUserid(usert.getUserid());
		pt.setNick(usert.getRealname());
		pt.setHeadpath(usert.getHeadpath());
		pt.setSmallimageurl(this.getImageurl());
		pt.setBigimageurl(this.getImageurl());
		pt.setZannum(0);
		pt.setReplynum(0);
		pt.setActivetime(BaseTools.getSystemTime());
		pt.setStatus(DataUsingState.USING.getState());
		pt.setUpdatetime(BaseTools.getSystemTime());
		pt.setCreatetime(BaseTools.getSystemTime());
		pt.setVersiont(1);
		this.postsTService.save(pt);
		this.setSucflag(true);
		return JSON;
	}

	/**
	 * 查询所有
	 * 
	 * @return
	 */
	@Action(value = "findAll", results = { @Result(name = "json", type = "json") })
	public String findAllPostsT() {
		Criterion criterion = Restrictions.eq("status", DataUsingState.USING.getState());
		List<PostsT> list = this.postsTService.findByCriteria(PostsT.class, criterion);
		if (!list.isEmpty()) {
			beanlists = new ArrayList<PostsT>();
			beanlists = list;
			this.setSucflag(true);
		}
		return JSON;
	}

	@Action(value = "findByPage", results = { @Result(name = "json", type = "json") })
	public String findPostsTByPage() {
//		Map<String, Object> params = ActionContext.getContext().getParameters();
//		for (Map.Entry<String, Object> entry : params.entrySet()) {
//			String key = entry.getKey();
//			Object value = entry.getValue();
//			System.out.println("key:" + key + " value: " + Arrays.toString((String[]) value));
//		}
		this.findDefaultAllPostsTN();
		return JSON;
	}

	public void processPostsTList(List<PostsT> list) {
		for (Iterator<PostsT> it = list.iterator(); it.hasNext();) {
			PostsT pt = it.next();
			Map<String, Object> cellMap = new HashMap<String, Object>();
			cellMap.put("id", pt.getId());
			cellMap.put("title", pt.getTitle());
			cellMap.put("content", pt.getContent());
			cellMap.put("zannum", pt.getZannum());
			cellMap.put("replynum", pt.getReplynum());
			cellMap.put("nick", pt.getNick());
			cellMap.put("loginname", pt.getLoginname());
			cellMap.put("updatetime", BaseTools.formateDbDate(pt.getUpdatetime()));
			cellMap.put("activetime", BaseTools.formateDbDate(pt.getActivetime()));
			cellMap.put("versiont", pt.getVersiont());
			cellMap.put("status", BaseEnums.DataUsingState.getName(pt.getStatus()));
			data.add(cellMap);
		}
	}

	public void findDefaultAllPostsTN() {
		int currentPage=1;
		if(this.getStart()!=0){
			currentPage=this.getStart()/this.getLength()==1?2:this.getStart()/this.getLength()+1;
		}
		int lineSize = this.getLength();
		Criterion criterion = Restrictions.ne("status", DataUsingState.DEL.getState());
		recordsFiltered = recordsTotal = this.postsTService.count(PostsT.class, criterion).intValue();
		List<PostsT> list = this.postsTService.findByCriteriaByPage(PostsT.class, criterion, Order.desc("updatetime"), currentPage, lineSize);
		this.processPostsTList(list);
	}

	/**
	 * 根据论坛id查询论坛信息
	 * 
	 * @return
	 */
	@Action(value = "find", results = { @Result(name = "json", type = "json") })
	public String findOnePostsT() {
		if (StringUtils.isBlank(this.getPostsId())) {
			return JSON;
		}
		PostsT postsT = this.postsTService.findByPK(PostsT.class, this.getPostsId());
		if (postsT != null) {
			bean = postsT;
			this.setSucflag(true);
		}
		return JSON;
	}

	/**
	 * 更新论坛数据
	 * 
	 * @return
	 */
	@Action(value = "update", results = { @Result(type = "json", name = "json") })
	public String updatePostsT() {
		PostsT postsT = this.postsTService.findByPK(PostsT.class, this.getPostsId());
		if (postsT != null) {
			postsT.setUpdatetime(BaseTools.getSystemTime());
			postsT.setStatus(this.getStatus());
			this.postsTService.update(postsT);
		}
		this.setSucflag(true);
		return JSON;
	}

	/**
	 * 删除论坛数据
	 * 
	 * @return
	 */
	@Action(value = "del", results = { @Result(type = "json", name = "json") })
	public String deletePostsT() {
		String id[] = StringUtils.split(this.getIds(), StaticKey.SPLITDOT);
		for (String s : id) {
			PostsT postsT = this.postsTService.findByPK(PostsT.class, s);
			if (postsT != null) {
				postsT.setStatus(DataUsingState.DEL.getState());
				postsT.setUpdatetime(BaseTools.getSystemTime());
				this.postsTService.update(postsT);
			}
		}
		this.setSucflag(true);
		return JSON;
	}

	private String title;
	private String content;
	private String imageurl;
	private String ids;
	private String postsId;
	private String status;
	private PostsT bean;
	private List<PostsT> beanlists;
	private List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
	private int recordsTotal = 0;
	private int recordsFiltered = 0;
	private int draw;// 表格控件请求次数
	/**
	 * 请求第几页
	 */
	private int start;
	/**
	 * 请求几条
	 */
	private int length;
	private boolean sucflag;

	public PostsTService getPostsTService() {
		return postsTService;
	}

	public void setPostsTService(PostsTService postsTService) {
		this.postsTService = postsTService;
	}

	public PostsT getBean() {
		return bean;
	}

	public void setBean(PostsT bean) {
		this.bean = bean;
	}

	public List<PostsT> getBeanlists() {
		return beanlists;
	}

	public void setBeanlists(List<PostsT> beanlists) {
		this.beanlists = beanlists;
	}

	public List<Map<String, Object>> getData() {
		return data;
	}

	public void setData(List<Map<String, Object>> data) {
		this.data = data;
	}

	public int getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(int recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public int getRecordsFiltered() {
		return recordsFiltered;
	}

	public void setRecordsFiltered(int recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public boolean isSucflag() {
		return sucflag;
	}

	public void setSucflag(boolean sucflag) {
		this.sucflag = sucflag;
	}

	public String getPostsId() {
		return postsId;
	}

	public void setPostsId(String postsId) {
		this.postsId = postsId;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

}
