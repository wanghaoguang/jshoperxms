package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.ArticleT;
import com.jshoperxms.service.ArticleTService;

@Service("articleTService")
@Scope("prototype")
public class ArticleTServiceImpl extends BaseTServiceImpl<ArticleT> implements ArticleTService {

}
