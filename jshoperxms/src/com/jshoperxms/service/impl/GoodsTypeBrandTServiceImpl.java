package com.jshoperxms.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.jshoperxms.action.mall.backstage.vo.GoodsTypeBrandsVo;
import com.jshoperxms.action.utils.BaseTools;
import com.jshoperxms.action.utils.json.GsonJson;
import com.jshoperxms.action.utils.statickey.StaticKey;
import com.jshoperxms.dao.BrandTDao;
import com.jshoperxms.dao.GoodsTypeBrandTDao;
import com.jshoperxms.entity.BrandT;
import com.jshoperxms.entity.GoodsTypeBrandT;
import com.jshoperxms.service.GoodsTypeBrandTService;

@Service("goodsTypeBrandTService")
@Scope("prototype")
public class GoodsTypeBrandTServiceImpl extends
		BaseTServiceImpl<GoodsTypeBrandT> implements GoodsTypeBrandTService {
	@Resource
	private GoodsTypeBrandTDao goodsTypeBrandTDao;
	@Resource
	private Serial serial;
	@Resource
	private BrandTDao brandTDao;
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void saveGoodsTypeBrands(String brands,
			String goodsTypeId, String goodsTypeName) {
		List<GoodsTypeBrandsVo>gtbs=GsonJson.parseJsonToData(brands, GoodsTypeBrandsVo.class);
		for(GoodsTypeBrandsVo gtb:gtbs){
			//1，增加品牌到brand表以获取brandid
			BrandT bt=new BrandT();
			bt.setId(this.serial.Serialid(Serial.BRAND));
			bt.setBrandname(gtb.getBrandname());
			bt.setCreatetime(BaseTools.getSystemTime());
			bt.setCreatorid(BaseTools.getAdminCreateId());
			bt.setDes(StaticKey.EMPTY);
			bt.setLogo(StaticKey.EMPTY);
			bt.setSmallLogo(StaticKey.EMPTY);
			bt.setSort(gtb.getSort());
			bt.setStatus(gtb.getStatus());
			bt.setUpdatetime(BaseTools.getSystemTime());
			bt.setWebsite(StaticKey.EMPTY);
			bt.setVersiont(1);
			this.brandTDao.save(bt);
			GoodsTypeBrandT gt=new GoodsTypeBrandT();
			gt.setGoodsTypeBrandTid(this.serial.Serialid(Serial.GOODSTYPEBRAND));
			gt.setBrandid(bt.getId());
			gt.setBrandname(bt.getBrandname());
			gt.setBrandlist(brands);
			gt.setCreatetime(bt.getCreatetime());
			gt.setCreatorid(bt.getCreatorid());
			gt.setGoodsTypeId(goodsTypeId);
			gt.setGoodsTypeName(goodsTypeName);
			gt.setStatus(bt.getStatus());
			gt.setUpdatetime(bt.getUpdatetime());
			gt.setVersiont(1);
			gt.setSort(bt.getSort());
			this.goodsTypeBrandTDao.save(gt);
		}
	}


}
