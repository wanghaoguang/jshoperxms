package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.BasicUserT;
import com.jshoperxms.service.BasicUserTService;

@Service("basicUserTService")
@Scope("prototype")
public class BasicUserTServiceImpl extends BaseTServiceImpl<BasicUserT> implements BasicUserTService {

}
