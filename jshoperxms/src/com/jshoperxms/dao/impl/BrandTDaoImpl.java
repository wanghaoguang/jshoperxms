package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.BrandTDao;
import com.jshoperxms.entity.BrandT;


@Repository("brandTDao")
public class BrandTDaoImpl extends BaseTDaoImpl<BrandT> implements BrandTDao {

	private static final Logger log = LoggerFactory.getLogger(BrandTDaoImpl.class);


}