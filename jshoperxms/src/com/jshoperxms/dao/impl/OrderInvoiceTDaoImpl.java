package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.OrderInvoiceTDao;
import com.jshoperxms.entity.OrderInvoiceT;

@Repository("orderInvoiceTDao")
public class OrderInvoiceTDaoImpl extends BaseTDaoImpl<OrderInvoiceT> implements OrderInvoiceTDao {

	private static final Logger log = LoggerFactory.getLogger(OrderInvoiceTDaoImpl.class);


}