package com.jshoperxms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the posts_reply_t database table.
 * 
 */
@Entity
@Table(name="posts_reply_t")
@NamedQuery(name="PostsReplyT.findAll", query="SELECT p FROM PostsReplyT p")
public class PostsReplyT implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	private String bigimageurl;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createtime;

	private String headpath;

	private String loginname;

	private String nick;

	private String postsid;

	private String replycontent;

	private String replyid;

	private String smallimageurl;

	private String status;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updatetime;

	private String userid;

	private int versiont;

	public PostsReplyT() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBigimageurl() {
		return this.bigimageurl;
	}

	public void setBigimageurl(String bigimageurl) {
		this.bigimageurl = bigimageurl;
	}

	public Date getCreatetime() {
		return this.createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public String getHeadpath() {
		return this.headpath;
	}

	public void setHeadpath(String headpath) {
		this.headpath = headpath;
	}

	public String getLoginname() {
		return this.loginname;
	}

	public void setLoginname(String loginname) {
		this.loginname = loginname;
	}

	public String getNick() {
		return this.nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getPostsid() {
		return this.postsid;
	}

	public void setPostsid(String postsid) {
		this.postsid = postsid;
	}

	public String getReplycontent() {
		return this.replycontent;
	}

	public void setReplycontent(String replycontent) {
		this.replycontent = replycontent;
	}

	public String getReplyid() {
		return this.replyid;
	}

	public void setReplyid(String replyid) {
		this.replyid = replyid;
	}

	public String getSmallimageurl() {
		return this.smallimageurl;
	}

	public void setSmallimageurl(String smallimageurl) {
		this.smallimageurl = smallimageurl;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getUpdatetime() {
		return this.updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public int getVersiont() {
		return this.versiont;
	}

	public void setVersiont(int versiont) {
		this.versiont = versiont;
	}

}