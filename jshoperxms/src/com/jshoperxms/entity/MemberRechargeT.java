package com.jshoperxms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the member_recharge_t database table.
 * 
 */
@Entity
@Table(name="member_recharge_t")
@NamedQuery(name="MemberRechargeT.findAll", query="SELECT m FROM MemberRechargeT m")
public class MemberRechargeT implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	private double balance;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createtime;

	private String memberid;

	private String membername;

	private String shopid;

	@Temporal(TemporalType.TIMESTAMP)
	private Date updatetime;

	private int versiont;

	public MemberRechargeT() {
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public double getBalance() {
		return this.balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public Date getCreatetime() {
		return this.createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public String getMemberid() {
		return this.memberid;
	}

	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}

	public String getMembername() {
		return this.membername;
	}

	public void setMembername(String membername) {
		this.membername = membername;
	}

	public String getShopid() {
		return this.shopid;
	}

	public void setShopid(String shopid) {
		this.shopid = shopid;
	}

	public Date getUpdatetime() {
		return this.updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public int getVersiont() {
		return this.versiont;
	}

	public void setVersiont(int versiont) {
		this.versiont = versiont;
	}

}